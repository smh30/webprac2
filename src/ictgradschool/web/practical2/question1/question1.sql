-- Complete your SQL here
ALTER TABLE m_songs
    DROP FOREIGN KEY m_songs_ibfk_1;
ALTER TABLE m_composer
    DROP FOREIGN KEY m_composer_ibfk_1,
    DROP FOREIGN KEY m_composer_ibfk_2;
ALTER TABLE m_lyricist
    DROP FOREIGN KEY m_lyricist_ibfk_1,
   DROP FOREIGN KEY m_lyricist_ibfk_2;
ALTER TABLE m_songgenre
    DROP FOREIGN KEY m_songgenre_ibfk_1,
DROP FOREIGN KEY m_songgenre_ibfk_2;

DROP TABLE IF EXISTS m_songs;
DROP TABLE IF EXISTS m_artists;
DROP TABLE IF EXISTS m_producer;
DROP TABLE IF EXISTS m_genre;
DROP TABLE IF EXISTS m_lyricist;
DROP TABLE IF EXISTS m_composer;
DROP TABLE IF EXISTS m_songgenre;



CREATE TABLE m_artists(
  id INT NOT NULL ,
  fname VARCHAR(40),
  lname VARCHAR(40),
  age INT,
  gender VARCHAR(30),
  nationality CHAR(2),
  PRIMARY KEY (id)
);

CREATE TABLE m_producer(
  id INT NOT NULL ,
  fname VARCHAR(40),
  lname VARCHAR(40),
  age INT,
  gender VARCHAR(20),
  nationality CHAR(2),
  PRIMARY KEY (id)
);


-- moved this down here in hopes that foreign key will work
CREATE TABLE m_songs(
  id INT NOT NULL ,
  title VARCHAR(200),
  producer_id INT,
  PRIMARY KEY (id),
  FOREIGN KEY (producer_id) REFERENCES m_producer (id)
);



CREATE TABLE m_genre(
  title VARCHAR(50) NOT NULL ,
  description VARCHAR(200),
  PRIMARY KEY (title)
);

CREATE TABLE m_lyricist(
  song_id INT,
  artist_id INT,
  PRIMARY KEY (song_id, artist_id),
  FOREIGN KEY (song_id) REFERENCES m_songs (id),
  FOREIGN KEY (artist_id) REFERENCES m_artists (id)
);

CREATE TABLE m_composer(
  song_id INT,
  artist_id INT,
  PRIMARY KEY (song_id, artist_id),
  FOREIGN KEY (song_id) REFERENCES m_songs (id),
  FOREIGN KEY (artist_id) REFERENCES m_artists (id)
);

CREATE TABLE m_songgenre(
  song_id INT,
  genre_title VARCHAR(50),
  PRIMARY KEY (song_id, genre_title),
  FOREIGN KEY (song_id) REFERENCES m_songs (id),
  FOREIGN KEY (genre_title) REFERENCES m_genre (title)
);

INSERT INTO m_artists VALUES (1, 'Hayley', 'Kiyoko', 25, 'Female', 'US'),
                             (2, 'George', 'Watsky', 31, 'Male', 'US'),
                             (3, 'Taylor', 'Swift', 29, 'Female', 'US');

INSERT INTO m_producer VALUES (1, 'Billy', 'Elliot', 12, 'Male', 'UK'),
                              (2, 'Sally', 'Hemsworth', 76, 'x', 'SE');

INSERT INTO m_songs VALUES (1, 'Helpless', 1),
                           (2, 'The best song in the world', 2),
                           (3, '16th symphony in B flat minor for piano and cymbals', 1);

INSERT INTO m_lyricist VALUES (1, 2),
                              (1, 3),
                              (2, 1);

INSERT INTO m_composer VALUES (1, 2),
                              (2, 1),
                              (2, 3),
                              (3, 2);

INSERT INTO m_genre VALUES ('pop', 'popular music'),
                           ('country', 'music from nashville, cowboys, etc'),
                           ('rap', 'a lot of words'),
                           ('instrumental', 'no words');

INSERT INTO m_songgenre VALUES (1, 'pop'),
                               (1, 'rap'),
                               (2, 'country'),
                               (3, 'instrumental'),
                               (2, 'rap');


