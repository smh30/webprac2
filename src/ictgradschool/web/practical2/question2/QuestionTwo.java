package ictgradschool.web.practical2.question2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class QuestionTwo extends HttpServlet {
    private String pdfDirname;
    private String pdfIcon;


    public void init() {
        pdfDirname = getServletConfig().getInitParameter("base-file-directory");
        pdfIcon = getServletConfig().getInitParameter("icon-file-name");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<FileEntry> entries = FileEntryDAO.getAllFileEntriesSorted();
        request.setAttribute("pdfDirname", pdfDirname);
        request.setAttribute("pdfIcon", pdfIcon);
        request.setAttribute("entries", entries);

        request.getRequestDispatcher("question2_render.jsp").forward(request,response);
    }
}
