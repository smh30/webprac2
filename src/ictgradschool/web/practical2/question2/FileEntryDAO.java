package ictgradschool.web.practical2.question2;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileEntryDAO {

    private static String url = "jdbc:mariadb://db.sporadic.nz/smh30";
    private static String user = "smh30";
    private static String pass = "SeaLionTalkativeTrip";

    public static List<FileEntry> getAllFileEntriesSorted() {
        List<FileEntry> entries = new ArrayList<>();

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) { }


        try (Connection conn = DriverManager.getConnection(url, user, pass)){
            try (Statement stmt = conn.createStatement()){
                try (ResultSet rs = stmt.executeQuery("SELECT * FROM practical_two_files")){
                    while(rs.next()){
                        FileEntry fileEntry = new FileEntry();
                        String path = rs.getString(1);
                        String name = rs.getString(2);
                        String title = rs.getString(3);
                        String desc = rs.getString(4);

                        fileEntry.setFileName(name);
                        fileEntry.setFilePath(path);
                        fileEntry.setFileTitle(title);
                        fileEntry.setFileDescription(desc);

                        entries.add(fileEntry);
                    }
                }
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        Collections.sort(entries);
        return entries;
    }
}
