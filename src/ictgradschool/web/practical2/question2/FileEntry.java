package ictgradschool.web.practical2.question2;

import java.io.Serializable;

public class FileEntry implements Serializable, Comparable<FileEntry> {
    private String fileName;
    private String filePath;
    private String fileTitle;
    private String fileDescription;

    public FileEntry(){
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    @Override
    public int compareTo(FileEntry other) {
        return (this.fileName).compareTo(other.fileName);
    }
}
