<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Question 2 - File List</title>

        <style type="text/css">
            /* You do not need to change these styles */
            a, dl {
                float: left;
            }

            div::after {
                content: "";
                clear: both;
                display: table;
            }
        </style>
    </head>
    <body>
        <h1>List of files from the ${pdfDirname} directory</h1>

        <c:forEach items="${entries}" var="entry">
        <div>
            <a href="${pdfDirname}/${entry.filePath}"><img src="${pdfDirname}/${pdfIcon}" title="${entry.fileName}" alt="pdf icon"></a>
            <dl><dt>${entry.fileTitle}</dt><dd>${entry.fileDescription}</dd></dl>
        </div>
        </c:forEach>
    </body>
</html>
